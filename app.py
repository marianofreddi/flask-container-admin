from flask import Flask, render_template, redirect, url_for
from flask_httpauth import HTTPBasicAuth
from werkzeug.security import generate_password_hash, check_password_hash
import docker

app = Flask(__name__)
auth = HTTPBasicAuth()
client = docker.from_env()

# Configuración de usuarios
users = {
    "maf": generate_password_hash("norompasnada")
}

@auth.verify_password
def verify_password(username, password):
    if username in users and check_password_hash(users.get(username), password):
        return username

@app.route('/')
def index():
    container = client.containers.get('my_nginx')
    container_status = container.status
    return render_template('index.html', status=container_status)

@app.route('/start')
@auth.login_required
def start_container():
    container = client.containers.get('my_nginx')
    if container.status != 'running':
        container.start()
    return redirect(url_for('index'))

@app.route('/stop')
@auth.login_required
def stop_container():
    container = client.containers.get('my_nginx')
    if container.status == 'running':
        container.stop()
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
